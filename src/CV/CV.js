export const CV = {
    hero: {
        name: 'Jordi',
        surname: 'Ollé Ballesté',
        city: 'Alcover / Tarragona',
        email: 'jordi@olle.com',
        birthDate: '11/03/1994',
        phone: '(+34) 678789890',
        image: 'https://media-exp1.licdn.com/dms/image/C4D03AQH2rSlMabTDPg/profile-displayphoto-shrink_800_800/0/1609012005189?e=1648684800&v=beta&t=7P3OdyPscS4_CApVlTb-CMFp4cMP1pjEG34lzJ4IhvQ',
        gitHub: 'https://github.com/jolle11',
        aboutMe: [
            {
                info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quos enim nesciunt ad dolore nostrum dolorem odio autem, mollitia impedit quod pariatur consectetur quidem sequilaboriosam minima, consequuntur voluptatem voluptatibus ratione dolor maxime assumenda aspernatur,voluptates recusandae laudantium. Distinctio, neque repudiandae veritatis ut aliquam excepturi beatae.',
            },
            {
                info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro vitae ducimus mollitia nobis! Quos enim nesciunt ad dolore nostrum dolorem odio autem, mollitia impedit quod pariatur consectetur quidem sequilaboriosam minima, consequuntur voluptatem voluptatibus ratione dolor maxime assumenda aspernatur,voluptates recusandae laudantium.',
            },
            {
                info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro vitae ducimus mollitia nobis! Quos enim nesciunt ad dolore nostrum dolorem odio autem, mollitia impedit quod pariatur consectetur quidem sequilaboriosam minima, consequuntur voluptatem voluptatibus ratione dolor maxime assumenda aspernatur. Distinctio, neque repudiandae veritatis ut aliquam excepturi beatae.',
            },
            {
                info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro vitae ducimus mollitia nobis! Mollitia impedit quod pariatur consectetur quidem sequilaboriosam minima, consequuntur voluptatem voluptatibus ratione dolor maxime assumenda aspernatur,voluptates recusandae laudantium. Distinctio, neque repudiandae veritatis ut aliquam excepturi beatae.',
            },
        ],
    },
    education: [
        {
            name: 'Master in physics',
            date: '1985-1986',
            where: 'MIT (Massachusetts Institute of Technology)',
        },
        {
            name: 'Aeronautical Engineering',
            date: '1981-1985',
            where: 'ATI Vaughn College',
        },
        {
            name: 'Mechanical Engineering',
            date: '1977-1981',
            where: 'Firearm College, Queens',
        },
    ],
    experience: [
        {
            name: 'Consultant',
            date: '01/01/2013 – Nowadays',
            where: 'S.H.I.E.L.D',
            description:
                'It builds the helicarriers used by S.H.I.E.L.D. It produces the Quinjets used by the Avengers.',
        },
        {
            name: 'CEO',
            date: '01/01/2000 – 28/02/2012',
            where: 'Stark Industries',
            description:
                'Manage the company, which is a multi-billion dollar multinational corporation that develops and manufactures advanced weapon and defense technologies. The company manufactures the armor worn by Iron Man and War Machine.',
        },
    ],
    languages: {
        language: 'English',
        wrlevel: 'Native',
        splevel: 'Native',
    },
    habilities: [
        'Robotics',
        'Robot Programming',
        'Physics',
        'Weaponery',
        'Engineer',
        'Money',
        'Dating',
        'Saving the world',
    ],
    volunteer: [
        {
            name: 'September Foundation',
            where: 'MIT',
            description:
                "The September Foundation is a program by Tony Stark to fund schools and young prodigies in their education. The foundation was named by Stark after a lyric from The Fantasticks song, 'Try to Remember,' which he heard his mother sing and play on the piano before her death.",
        },
        {
            name: 'Damage Control',
            where: 'U.S.A.',
            description:
                'The United States Department of Damage Control, occasionally known as the DODC, is a department of the United States of America. Initially a subsidiary of S.H.I.E.L.D., Damage Control was an organization specializing in post-battle clean-up. Following the Battle of New York, Damage Control was made into an executive branch of the United States government, and in a joint venture with Stark Industries, was tasked with acquiring alien and other dangerous artifacts along with cleaning up damages caused by enhanced individuals.',
        },
    ],
};
