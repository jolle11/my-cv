import './App.scss';
import { useState } from 'react';
import { CV } from './CV/CV';
import { Hero, About, Education, Experience, More } from './components';

const { hero, education, experience, languages, habilities, volunteer } = CV;

function App() {
    const [showEducation, setShowEducation] = useState(true);
    return (
        <div className="app container">
            <div className="app__hero">
                <Hero hero={hero}></Hero>
                <About hero={hero}></About>
            </div>
            <div className="app__buttons">
                <button className="app__button btn btn-outline-success" onClick={() => setShowEducation(true)}>
                    Education
                </button>
                <button className="app__button btn btn-outline-success" onClick={() => setShowEducation(false)}>
                    Experience
                </button>
            </div>
            <div className="app__eduandexp container">
                {showEducation ? (
                    <Education education={education}></Education>
                ) : (
                    <Experience experience={experience}></Experience>
                )}
            </div>
            <More languages={languages} habilities={habilities} volunteer={volunteer}></More>
        </div>
    );
}

export default App;
