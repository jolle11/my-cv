import './Education.scss';

const Education = ({ education }) => {
    return (
        <div className="education">
            <h2 className="education__title">Education</h2>
            <div>
                {education.map((item) => {
                    return (
                        <div key={JSON.stringify(item)}>
                            <h4 className="education__name">{item.name}</h4>
                            <p className="education__where">{item.where}</p>
                            <small className="education__when text-success">{item.date}</small>
                        </div>
                    );
                })}
            </div>
        </div>
    );
};

export default Education;
