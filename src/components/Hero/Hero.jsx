import './Hero.scss';

const Hero = ({ hero }) => {
    return (
        <div className="hero">
            <img src={hero.image} alt="" className="hero__image" />
            <div className="hero__card">
                <h2 className="hero__title">
                    {hero.name} {hero.surname}
                </h2>
                <p>{hero.city}</p>
                <p>{hero.birthDate}</p>
                <p>
                    <a href={'mailto:' + hero.email}>jordi@olle.com</a>
                </p>
                <p>{hero.phone}</p>
                <p>
                    <a href={hero.github}>GitHub</a>
                </p>
            </div>
        </div>
    );
};

export default Hero;
