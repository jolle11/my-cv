import './More.scss';

const More = ({ languages: { language, wrlevel, splevel }, habilities, volunteer }) => {
    console.log(volunteer);
    return (
        <div className="more">
            <div className="languages">
                <h2 className="languages__title">Languages 🌐</h2>
                <p>Language: {language}</p>
                <p>Writing level: {wrlevel}</p>
                <p>Speaking level: {splevel}</p>
            </div>
            <div className="habilities">
                <h2 className="habilities__title">Habilities 🥷🏻</h2>
                {habilities.map((item) => {
                    return (
                        <div className="habilities__list" key={JSON.stringify(item)}>
                            <li>{item}</li>
                        </div>
                    );
                })}
            </div>
            <div className="volunteer">
                <h2 className="volunteer__title">Volunteer 👫</h2>
                {volunteer.map((item) => {
                    return (
                        <div className="volunteer__list" key={JSON.stringify(item)}>
                            <h4>{item.name}</h4>
                            <p>{item.where}</p>
                            <small>{item.description}</small>
                        </div>
                    );
                })}
            </div>
        </div>
    );
};

export default More;
