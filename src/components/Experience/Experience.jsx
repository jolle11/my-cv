import './Experience.scss';

const Experience = ({ experience }) => {
    return (
        <div className="experience">
            <h2 className="experience__title">Experience</h2>
            <div>
                {experience.map((item) => {
                    return (
                        <div key={JSON.stringify(item)}>
                            <h4 className="experience__name">{item.name}</h4>
                            <p className="experience__where">{item.where}</p>
                            <p className="experience__when text-success">{item.date}</p>
                            <small className="experience__description">{item.description}</small>
                        </div>
                    );
                })}
            </div>
        </div>
    );
};

export default Experience;
