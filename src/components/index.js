// IMPORT COMPONENTS
import Hero from './Hero/Hero';
import About from './About/About';
import Education from './Education/Education';
import Experience from './Experience/Experience';
import More from './More/More';
// EXPORT COMPONENTS
export { Hero, About, Education, Experience, More };
