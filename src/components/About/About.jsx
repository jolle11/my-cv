import './About.scss';

const About = ({ hero: { aboutMe } }) => {
    return (
        <div className="about">
            <h2 className="about__title">About Me</h2>
            <div className="about__list">
                {aboutMe.map((item) => {
                    return (
                        <div key={JSON.stringify(item)}>
                            <p>{item.info}</p>
                        </div>
                    );
                })}
            </div>
        </div>
    );
};

export default About;
